package org.jidcoo.formsys.starter;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.util.component.LifeCycle;


import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.*;

/**
 * 启动器目录结构
 * spring-starter.jar
 * config
 *  --apps
 * webApp
 *  --app.war
 */
public class Starter {
    public static final Logger LOGGER ;

    static {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String trace = sdf.format(new Date().getTime());
        LOGGER = Logger.getLogger("org.jidcoo.formsys.starter.Starter");
        LOGGER.setLevel(Level.ALL);
        FileHandler fileHandler = null;
        try {
            File file=new java.io.File("log/starter_logs/");
            if(!file.exists()){
                file.mkdirs();
            }
            fileHandler = new FileHandler("log/starter_logs/java_"+trace+".log",true ); // 只用一个文件记录日志
        } catch (IOException e) {
            e.printStackTrace();
        }
        fileHandler.setFormatter(new Formatter() {
            @Override
            public String format(LogRecord arg0) {
                SimpleDateFormat df = new SimpleDateFormat("yyyy年MM月dd日 HH:mm:ss");//设置日期格式
                return df.format(new Date()) + "\t" + arg0.getLoggerName() + "\t" + arg0.getLevel().getName()  + "\t" + arg0.getMessage() + "\n\r\n";
            }
        });
        fileHandler.setLevel(Level.ALL);
        LOGGER.addHandler(fileHandler);
    }

    public static Server start(AbstractLifeCycle.AbstractLifeCycleListener listener) {
        LOGGER.info("[Spring Starter] 启动器启动中");
        String appPath=getAppPath();
        Map<String,String> appMap=readStarterConfig(appPath);

        if(appMap!=null&&appMap.size()>0){
            LOGGER.info("[Spring Starter] 正在启动应用，使用端口：8080");
            try {
                Server server = MultipleServicesAndOnePortStarter.startApps(appMap, 8080, appPath,new AbstractLifeCycle.AbstractLifeCycleListener() {
                    @Override
                    public void lifeCycleFailure(LifeCycle event, Throwable cause) {
                        super.lifeCycleFailure(event, cause);
                        LOGGER.info("[Spring Starter] 应用启动失败："+cause.getMessage());
                        if(listener!=null){
                            listener.lifeCycleFailure(event,cause);
                        }
                    }
                    @Override
                    public void lifeCycleStarted(LifeCycle event) {
                        super.lifeCycleStarted(event);
                        LOGGER.info("[Spring Starter] 应用成功启动");
                        if(listener!=null){
                            listener.lifeCycleStarted(event);
                        }
                    }

                    @Override
                    public void lifeCycleStarting(LifeCycle event) {
                        super.lifeCycleStarting(event);
                        LOGGER.info("[Spring Starter] 应用正在启动");
                        if(listener!=null){
                            listener.lifeCycleStarting(event);
                        }
                    }

                    @Override
                    public void lifeCycleStopped(LifeCycle event) {
                        super.lifeCycleStopped(event);
                        LOGGER.info("[Spring Starter] 应用成功关闭");
                        LOGGER.info("[Spring Starter] 正在清除资源文件");
                        if(deleteTmpFile(appPath)){
                            LOGGER.info("[Spring Starter] 资源文件清除成功");
                        }else{
                            LOGGER.info("[Spring Starter] 资源文件清除失败");
                        }
                        if(listener!=null){
                            listener.lifeCycleStopped(event);
                        }
                    }

                    @Override
                    public void lifeCycleStopping(LifeCycle event) {
                        super.lifeCycleStopping(event);
                        LOGGER.info("[Spring Starter] 应用正在关闭");
                        if(listener!=null){
                            listener.lifeCycleStopping(event);
                        }
                    }
                });
                return server;
            } catch (Exception e) {
                LOGGER.info("[Spring Starter] 启动出错");
                e.printStackTrace();
                return null;
            }
        }else{
            LOGGER.info("[Spring Starter] 应用池所含应用为0,没有需要启动的应用");
            LOGGER.info("[Spring Starter] 退出Spring启动器");
            return null;
        }
    }

    private static boolean deleteTmpFile(String appPath) {
        File file=new File(appPath+"\\webApp\\root\\");
        if(!file.exists()){
            return true;
        }
        //delete the dir and the files
        deleteFolder(file.getAbsolutePath());
        return true;
    }


     static boolean deleteFolder(String url) {
         File file = new File(url);
         //LOGGER.info("正在删除："+url);
         if (!file.exists()) {
             return false;
         }
         if (file.isFile()) {
             file.delete();
             return true;
         } else {
             File[] files = file.listFiles();
             for (int i = 0; i < files.length; i++) {
                 String root = files[i].getAbsolutePath();
                 //LOGGER.info("正在删除："+root);
                 deleteFolder(root);
             }
             file.delete();
             return true;
         }
     }

    /**
     * 读取应用配置文件
     * 读取war文件 解压到webapp/root
     * 文件名加入map
     * @param appPath
     * @return
     */
    private static Map<String, String> readStarterConfig(String appPath) {
        Map<String,String> appMap=new HashMap<>();
        String webAppRootPath=appPath+"\\webApp\\";
        String configPath=appPath+"\\config\\apps";
        LOGGER.info("[Spring Starter] App Path :"+appPath);
        LOGGER.info("[Spring Starter] webAppRootPath Path :"+webAppRootPath);
        LOGGER.info("[Spring Starter] ConfigFile Path :"+configPath);
        java.io.File file=new File(configPath);
        if(!file.exists()){
            LOGGER.info("[Spring Starter] apps应用配置文件不存在");
            LOGGER.info("[Spring Starter] 退出Spring启动器");
            return null;
        }
        LOGGER.info("[Spring Starter] 正在读取apps应用配置文件");
        try {
            FileInputStream inputStream=new FileInputStream(file);
            BufferedReader reader=new BufferedReader(new InputStreamReader(inputStream,"utf-8"));
            String lineConfig=null;
            while ((lineConfig=reader.readLine())!=null){
                String[] s = lineConfig.split(" ",2);
                if(s.length!=2){
                    LOGGER.info("[Spring Starter] apps应用配置文件读取错误：行配置格式有误，格式： url appWarName");
                    LOGGER.info("[Spring Starter] 退出Spring启动器");
                    return null;
                }
                if(new File(webAppRootPath+s[1]+".war").exists()){
                    appMap.put(s[0],webAppRootPath+s[1]+".war");
                    LOGGER.info("[Spring Starter] 应用："+s[1]+",成功加入应用池");
                }else{
                    LOGGER.info("[Spring Starter] apps配置行："+lineConfig+",war包不存在，跳过此配置行");
                    continue;
                }
            }
            LOGGER.info("[Spring Starter] 读取apps应用配置文件完成");
        } catch (FileNotFoundException e) {
            LOGGER.info("[Spring Starter] apps应用配置文件不存在");
            LOGGER.info("[Spring Starter] 退出Spring启动器");
            return null;
        } catch (UnsupportedEncodingException e) {
            LOGGER.info("[Spring Starter] apps应用配置文件读取错误：UnsupportedEncodingException");
            LOGGER.info("[Spring Starter] 退出Spring启动器");
            return null;
        } catch (IOException e) {
            LOGGER.info("[Spring Starter] apps应用配置文件读取错误：IOException");
            LOGGER.info("[Spring Starter] 退出Spring启动器");
            return null;
        }
        return appMap;
    }

    private static String getAppPath() {
         return System.getProperty("user.dir");
    }


    public static void main(String[] args) {
         start(null);
    }



}

package org.jidcoo.formsys.starter;


import org.apache.jasper.servlet.JasperInitializer;
import org.apache.tomcat.InstanceManager;
import org.apache.tomcat.SimpleInstanceManager;
import org.eclipse.jetty.annotations.ServletContainerInitializersStarter;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.plus.annotation.ContainerInitializer;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.webapp.WebAppContext;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Jetty 多服务公用一个端口启动器
 */
public class MultipleServicesAndOnePortStarter {

	static {
		System.setProperty("org.eclipse.jetty.server.Request.maxFormContentSize", String.valueOf(Integer.MAX_VALUE));
		System.setProperty("org.eclipse.jetty.server.Request.maxFormKeys", String.valueOf(Integer.MAX_VALUE));
	}


	public static Server startApps(Map<String,String> apps, int port, String rootPath, AbstractLifeCycle.AbstractLifeCycleListener lifeCycleListener) throws Exception {
		if(apps!=null&&apps.size()>0){
			Server server=new Server(port);
			server.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize",-1);
			if(lifeCycleListener!=null){
				server.addLifeCycleListener(lifeCycleListener);
			}
			int i=0;
			ContextHandlerCollection contexts = new ContextHandlerCollection();
			Iterator<Map.Entry<String, String>> iterator = apps.entrySet().iterator();
		    while (iterator.hasNext()){
				Map.Entry<String, String> appConfig = iterator.next();
				String warAbsolutePath=appConfig.getValue();
				String contextPath=appConfig.getKey();
				if(contextPath!=null&&warAbsolutePath!=null){
					System.out.println("-----------------------------");
					System.out.println("启动器扫描到一个Server：");
					System.out.println("ContextPath："+contextPath);
					System.out.println("War："+warAbsolutePath);
					WebAppContext app=new WebAppContext();
					HttpConfiguration httpConfig = new HttpConfiguration();
					app.setContextPath(contextPath);
					File file=new File(rootPath+"\\webApp\\root\\");
					if(!file.exists()){
						file.mkdirs();
					}
					//If true the temp directory for this webapp will be kept when the webapp stops. Otherwise, it will be deleted
					app.setPersistTempDirectory(false);//当应用停止后删除资源文件
					app.setTempDirectory(file);
					app.setConfigurationDiscovered(true);
					app.setParentLoaderPriority(true);
					app.setWar(warAbsolutePath);
					app.setAttribute("org.eclipse.jetty.server.Request.maxFormContentSize",-1);
					app.setAttribute("org.eclipse.jetty.containerInitializers" , Arrays.asList (
							new ContainerInitializer(new JettyJasperInitializer(), null)));
					app.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
							".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/[^/]*taglibs.*\\.jar$");
					app.setAttribute(InstanceManager.class.getName(), new SimpleInstanceManager());
					app.addBean(new ServletContainerInitializersStarter(app), true);
					contexts.addHandler(app);
					i++;
					System.out.println("ContextPath："+contextPath+",注册成功");
					System.out.println("-----------------------------");
				}
			}
			server.setHandler(contexts);
		    System.out.println("启动器扫描Server完成，发现Server个数："+i+"\n\n正在启动Server，启动端口为："+port+"，请稍等…………");
			server.start();
			return server;
		}
		return null;
	}
}
